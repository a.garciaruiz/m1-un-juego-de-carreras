# **Un juego de carreras**

Dejo [aquí](https://youtu.be/RKdN50WfBoU) el enlace al vídeo de demostración.

## **Cómo jugar**

<div style="text-align: justify"> 

Lo primero que aparecera al darle a Play será el menú principal. Este menú dispone de dos botones: *Track Selection* y *Quit*. El botón *Track Selection* abrirá un panel de selección de pista para escoger en qué circuito queremos correr. En cambio, el botón *Quit* cerrará la aplicación.  

Dentro del menú de selección de pista, tendremos las imágenes correspondientes a los circuitos en los que podemos jugar y una flecha para volver al menú principal. Al pulsar sobre uno de ellos se cargará la escena pertinente. 

<p>
<img style="align: center" width="210px"src="images/main_menu.png") />
<img style="align: center" width="210px"src="images/track_selection.png") />
</p>

Una vez dentro del circuito, podemos controlar el coche mediante las flechas del teclado, pausar el juego pulsando la tecla **esc** y restablecerlo mediante el botón *Resume*, reiniciar la carrera mediante el botón *Restart* o volver al menú principal mediante el botón *Exit*. 

Lo mismo ocurre al terminar la carrera con el panel de finalizado. Podemos intentarlo de nuevo usando el botón *Try Again*, ver la repetición de la carrera mediante *Watch Replay* o volver al menú principal con el botón *Exit*. 

<p>
<img style="align: center" width="210px"src="images/pause_menu.png") />
<img style="align: center" width="210px"src="images/finish_menu.png") />
</p>

Un control de juego no especificado es el uso de la barra espaciadora. Para poder ver la repetición implementada con Cinemachine, se debe pulsar la tecla **espacio** para cambiar de una cámara a otra. El comportamiento por defecto es la repetición con la cámnara de juego.  

</div>

## **Estructura del juego**

<div style="text-align: justify"> 

Todo el juego está implementado de tal forma que pueda ser ampliable en un futuro. Esto implica que se ha dedicado bastante tiempo a hacer refactors y evitar la repetición de código, así como a incentivar la reutilización del mismo. 

Esto se traduce en scripts de corta duración que pueden ser reutilizables en otras partes del juego. Se han creado clases que sencillamente sirven para ser instanciadas y evitar redundancias innecesarias. 

A continuación se detalla por bloques cada una de las funcionalidades del juego y los scripts asociados:

> ### **Scripts Genéricos**
> En este apartado se detallan únicamente los scripts que se han implementado para ser reutilizables en múltiples clases. Esto dota al proyecto de una mayor modularidad. 
> 
> **1. Update Displays:** Clase genérica cuyos métodos reciben por parámetro el display de tiempo a actualizar y la información de tiempo. Sus métodos son:  
> 
>    1. `public static void UpdateMinsDisplay(GameObject display, int mins){}`: Actualiza el contador de minutos.  
> 
>    2. `public static void UpdateSecsDisplay(GameObject display, int secs){}`: Actualiza el contador de segundos.
>    3. `public static void UpdateMsDisplay(GameObject display, float ms){}`: Actualia el contador de milisegundos.
>    4. `public static void UpdateBestDisplay(GameObject[] display, int mins, int secs, float ms){}`: Actualiza por completo el contador de mejor tiempo llamando a los tres métodos anteriores y pasando por parámetro el display de mejor tiempo.
>   
> **2. Times:** Clase útil para instanciar valores de información de tiempo en minutos, segundos y milisegundos en cualquier otra clase.  
>   
> **3. Scenes:** Contiene los strings de las escenas del juego para acceder desde otras clases.  
>   
> **4. Listener Methods:** Contiene los métodos de todos los listeners del juego:  
>    1. `public static void ResumeGame(GameObject obj, AudioSource[] audio){}`.  
> 
>    2. `public static void ChangeScene(string scene){}`: Encargada de cambiar de escena de juego.  
>    3. `public static void ChangePanel(GameObject activeObj, GameObject inactiveObj){}`: Encargada de activar un nuevo panel a la vez que se desactiva el actual.
>    4. `public static void QuitApp(){}`: Cierra la aplicación.
>    5. `public static void RemoveListeners(List<Button> buttons){}`: Elimina los listeners de la lista de botones facilitada por parámetro.
> 
> **5. Ghost Data:** Clase útil para instanciar posiciones y rotaciones del coche. Permite crear listas de tipo Ghost Data para almacenar y reproducir el contenido en la repetición y el coche fantasma.  
> 
> **6. Files:** Contiene los nombres de los ficheros en los que se guarda la información del juego. Esto permite acceder desde otras clases sin cambiar una a una el nombre del fichero.  

> ### **Gestión de menús y escenas**  
> 
> **1. Menu Manager:** Encargado de gestionar el menú principal del juego. Contiene los listeners asociados a los botones del menú. Activa y desactiva los paneles de selección de circuito e inicio del juego. Permite cerrar la aplicación y seleccionar el circuito, además de cargar la escena de juego correspondiente.  
> 
>    1. `trackSelectionButt.onClick.AddListener(() => ListenerMethods.ChangePanel(startMenu, tracksMenu));`  
> 
>    2. `quitButt.onClick.AddListener(() => ListenerMethods.QuitApp());`
>    3. `backButton.onClick.AddListener(() => ListenerMethods.ChangePanel(tracksMenu, startMenu));`
>    4. `road1.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.road1Scene));`  
> 
> **2. Pause Manager:** Gestiona el menú de pausa del juego. Activa el panel del pausa al pulsar la tecla **esc** y para el tiempo de juego con `Time.timeScale = 0;`. Contiene los listeners de los botones para continuar jugando, empezar de nuevo o volver al menú principal.   
> 
>    1. `resumeButton.onClick.AddListener(() => ListenerMethods.ResumeGame(pausePanel, carAudio));`  
> 
>    2. `restartButton.onClick.AddListener(() => ListenerMethods.ChangeScene(scene.name));`
>    3. `exitButton.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.mainScene));`  
> 
> **3. Race Finished:** Activa el panel de final de carrera. Nos permite reiniciar, ver la repetición o salir al menú principal. *Este script se detalla con más precisión en los próximos apartados*. 
>    1. `restart_button.onClick.AddListener(() => ListenerMethods.ChangeScene(scene.name));`  
> 
>    2. `replay_button.onClick.AddListener(() => EnterReplay());`
>    3. `exit_button.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.mainScene));`

> ### **Gestión de vueltas y tiempo**  
> 
> **1. Countdown:** Implementa una coroutine de cuenta atrás que congela los controles del coche y el fantasma hasta que termina. Sólo sirve para los inicios de la carrera: 
> 
> **2. Laptime Manager:** Gestiona los tiempos de la vuelta actual. Usa la clase UpdateDisplays para actualizar el display.  
>  
> **3. Lap Completed:** Asociada a un trigger en la línea de meta que detecta cuando el coche la cruza. Resetea el contador de la vuelta actual y actualiza los displays del conjunto de vueltas, así como el mejor tiempo. Para ello hace uso de la clase Update Displays.  
> 
> **4. Lap Counter:** Actualiza el contador de vueltas cuando cruzamos la línea de meta hasta un máximo definido por el total de vueltas de la carrera.

> ### **Gestión de la carrera**  
> 
> **1. Activate Next Trigger:** Script que sirve para detectar cuando cruzamos un checkpoint y activar el siguiente. Evita coger atajos no permitidos. Si no pasamos por todos los checkpoints, la vuelta no cuenta. 
> 
> ```
> public class ActivateNextTrigger : MonoBehaviour
> {
>       public GameObject[] triggers;
>       private void OnTriggerEnter(Collider other)
>       {
>           if (other.gameObject.CompareTag("Player"))
>           {
>               triggers[0].SetActive(false);
>               triggers[1].SetActive(true);
>           }
>       }
> }
> ```
> 
> **2. Ground Detection:** Detecta mediante raycasing en qué superficie estamos. Si nos salimos de la carretera, aumenta el drag del Rigidbody para que el coche vaya más lento. También inicia un contador para evitar que nos quedemos estancados o nos salgamos fuera del circuito por más de 5 segundos.
> 
> ```
>     private void FixedUpdate() {
>           activeCheckpoint = GameObject.FindGameObjectWithTag("Checkpoint");
>           RaycastHit hit;
>           if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 10, roadLayer))
>           {
>               rb.drag = 0.1f;
>               timer = 0;
>           } else if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 10, terrainLayer))
>           {
>                rb.drag = 0.5f;
>               timer += Time.deltaTime;
>               if (timer >= 5)
>               {
>                   car.transform.position = activeCheckpoint.transform.position;
>                   car.transform.rotation = activeCheckpoint.transform.rotation;
>                   timer = 0;
>               }
>           }    
>           else
>           {
>                rb.AddForce(rb.mass * Physics.gravity);
>           }
>       }  
> ```   
> **3. Race Finished:** Al entrar en el trigger, se comprueba si es la última vuelta y se acciona el panel de finalización con sus correspondientes botones. Activamos la nueva cámara, la inteligencia artificial y desactivamos los controles del coche. 
> 
> ```
> private void OnTriggerEnter(Collider other)
>    {
>        if (LapCounter.lap == LapCounter.max_lap)
>        {
>            race_finished = true;
>
>            Debug.Log("STOP RECORDING");
>            GhostManager.shouldRecord = false;
>            Debug.Log("STOP PLAYING");
>            GhostManager.shouldPlay = false;
>
>            finish_cube.SetActive(true);
>            finish_panel.SetActive(true);
>
>           EnableAI();
>            DisableCarSettings();
>
>            main_camera.GetComponent<AudioListener>().enabled = false;
>            finish_camera.SetActive(true);
>            main_camera.SetActive(false);
>        }
>    }
> ```

> ### **Repetición y fantasma**
> **1. Ghost Manager:** Script muy similar al de la actividad del coche fantasma. Hay algunas modificaciones al respecto para adaptarlo al juego.  
>   
> En update comprobamos si debemos grabar los datos y si estamos o no en modo replay. Los flags de las condiciones **grabar** y **reproducir** se declaran como static para acceder desde otras clases y modificarlos según convenga. En caso que se cumpla la condición, almacenamos los datos en una lista de tipo GhostData.  
> 
> ```
>        if (shouldRecord && ReplayManager.replayMode == false)
>        {
>            // A cada frame incrementamos el tiempo transcurrido 
>            totalRecordedTime += Time.deltaTime;
>            currenttimeBetweenSamples += Time.deltaTime;
>
>            if (currenttimeBetweenSamples >= timeBetweenSamples)
>            {
>                // Guardamos la información para el fantasma
>                ghostData.Add(new GhostData(carToRecord.transform.position, carToRecord.transform.rotation));
>                // Dejamos el tiempo extra entre una muestra y otra
>                currenttimeBetweenSamples -= timeBetweenSamples;
>            }
>        }
> ```
> 
> La lectura de los datos se realiza de forma similar a la actividad, excepto que en vez de usar Scriptable Objects, se hace uso de un JSON que almacena los datos extraidos anteriormente. Los datos de las posiciones se leen de la siguiente forma:
> 
> ```
> nextPosition = LoadData.ghostData[currentSampleToPlay].ghostPos;
> nextRotation = LoadData.ghostData[currentSampleToPlay].ghostRot;
> ```
> 
> Donde LoadData es la clase encargada de cargar los datos de los JSON y ghostData la lista que contiene los datos de posición y rotación.  
> 
> Por último, se declara un método público que actualiza el archivo JSON que contiene la información del fantasma. Esto permite acceder desde el contador de vueltas para actualizar los datos sólo si se ha producido un nuevo mejor tiempo:  
> 
> ```
>    public static void UpdateGhost()
>    {
>        FileHandler.SaveToJSON<GhostData>(ghostData, Files.ghostDataFile);
>    }
> ```  
> 
> **2. Replay Manager:** Funcionamiento muy similar al Ghost Manager. La diferencia principal es que en vez de limpiar la lista de Ghost Data tras cada vuelta, esta almacena los datos de la carrera al completo. Por último, comprueba si estamos en Replay Mode para reproducir con exactitud el resultado de la carrera.  
>   
> Una vez finalizada la repetición, volvemos al menú de finalización de carrera con el coche controlado por la inteligencia artifical y la cámara orbitando alrededor.

> ### **Gestión de datos**
> Para almacenar los datos del juego se ha utilizado la clase File Handler. Dicha clase contiene otra clase más, llamada JsonHelper, que convierte de string a json y viceversa. Una vez convertidos los datos, los almacena en el array **Items[]**. 
> 
> ```
> public static class JsonHelper
>{
>    public static T[] FromJson<T>(string json)
>    {
>        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
>        return wrapper.Items;
>    }
>
>    public static string ToJson<T>(T[] array)
>    {
>        Wrapper<T> wrapper = new Wrapper<T>();
>        wrapper.Items = array;
>        return JsonUtility.ToJson(wrapper);
>    }
>
>    public static string ToJson<T>(T[] array, bool prettyPrint)
>    {
>        Wrapper<T> wrapper = new Wrapper<T>();
>        wrapper.Items = array;
>        return JsonUtility.ToJson(wrapper, prettyPrint);
>    }
>
>    [Serializable]
>    private class Wrapper<T>
>    {
>        public T[] Items;
>    }
>}
> ```
> 
> Podemos usar estos métodos para convertir los datos y guardarlos u obtenerlos de la siguiente manera: 
> 
> ```
> public static class FileHandler
>{
>    public static void SaveToJSON<T> (List<T> toSave, string fileName) {
>        string content = JsonHelper.ToJson<T>(toSave.ToArray());
>        WriteFile(GetPath(fileName), content);
>    }
>
>   public static List<T> ReadFromJSON<T>(string fileName) {
>        string content = ReadFile(GetPath(fileName));
>        if(string.IsNullOrEmpty(content) || content == "{}")
>        {
>            return new List<T>();
>        }
> 
>       List<T> res = JsonHelper.FromJson<T>(content).ToList();
>        return res;
>    }
>
>    public static string GetPath(string fileName)
>    {
>        return Application.persistentDataPath + "/" + fileName;
>    }
>
>    private static void WriteFile(string path, string content) {
>        FileStream fileStream = new FileStream(path, FileMode.Create);
>
>        using (StreamWriter writer = new StreamWriter(fileStream))
>        {
>            writer.Write(content);
>        }
>    }
>
>    private static string ReadFile(string path)
>    {
>        if (File.Exists(path))
>        {
>            using (StreamReader reader = new StreamReader(path))
>            {
>                string content = reader.ReadToEnd();
>                return content;
>            }
>        }
>        return "";
>    }
>}
>```
> Para guardar o leer los datos sencillamente llamamos a los métodos de la siguiente manera:
> 
> `FileHandler.SaveToJSON<T>(List<T>, File_Name);`
> 
> Donde T es el típo genérico que hace referencia al tipo de datos que contiene nuestra lista. 
> 
> Al iniciar el juego, lo primero que hacemos es comprobar la existencia de los ficheros y, sí existen, cargamos los datos para reproducir el fantasma y mostrar el mejor tiempo adquirido hasta la fecha. Para ello, se usa la clase Load Data: 
> 
> ```
> public class LoadData : MonoBehaviour
>{
>    public static List<Times> bestTimes;
>    public static List<GhostData> ghostData;
>    [SerializeField] private GameObject[] best_display;
>
>    void Start()
>    {
>        if (File.Exists(FileHandler.GetPath(Files.timesFile)))
>        {
>            bestTimes = FileHandler.ReadFromJSON<Times>(Files.timesFile);
>            UpdateDisplays.UpdateBestDisplay(best_display, bestTimes[0].min, bestTimes[0].sec, bestTimes[0].ms);
>        }
>
>        if (File.Exists(FileHandler.GetPath(Files.ghostDataFile)))
>        {
>            ghostData = FileHandler.ReadFromJSON<GhostData>(Files.ghostDataFile);
>        }
>    }
>}

> ### **Sistema de cámaras**
> Para el control de cámaras de juego hay diferentes scripts. Todos ellos se detallan a continuación:  
> 
> **1. Stable Camera:** Adjunta a un cubo invisible situado encima del coche. Se asegura que la cámara siempre mira a la cara trasera del cubo y nos da sensación de fluidez durante el juego. 
> 
> ```
>     void Update()
>    {
>        _carX = _car.transform.eulerAngles.x;
>        _carY = _car.transform.eulerAngles.y;
>        _carZ = _car.transform.eulerAngles.z;
>
>        transform.eulerAngles = new Vector3(_carX - _carX, _carY, _carZ - _carZ);
>    }
> ```
> 
> **2. Finish Anim:** Se activa al terminar la carrera. Está adjunta a otro cubo invisible y orbita alrededor de él. Hereda de Stable Camera, aunque esta vez se aplica una rotación:
> 
> ```
>     void Update()
>    {
>        transform.Rotate(_carX, 1, _carZ, Space.Self);
>    }
> ```
> 
> **3. Cinemachine Manager:** Al pulsar la barra espaciadora, desactiva la cámara principal del juego para mostrar la repetición mediante cámaras viruales del package Cinemachine. Para ello, se han creado 4 cámaras virtuales y se alterna la prioridad de forma indefinida en el tiempo: 
> 
> ```
> void Update()
>    {
>        if (ReplayManager.replayMode)
>        {
>            if(main_camera.activeSelf == true)
>            {
>                cinemachine_brain.GetComponent<AudioListener>().enabled = false;
>            }
>            else
>            {
>                cinemachine_brain.GetComponent<AudioListener>().enabled = true;
>            }
>
>            if (Input.GetKeyDown(KeyCode.Space))
>            {
>                main_camera.SetActive(!active);
>                cinemachine_brain.SetActive(active);
>                active = !active;
>            }
>        }
>    }
>
>    IEnumerator SwitchPriority()
>    {
>        while (true)
>        {
>            if (index >= 4)
>            {
>                index = 0;
>            }
>
>            foreach (CinemachineVirtualCamera vcam in vcams)
>            {
>                vcam.Priority = 0;
>            }
>            vcams[index].Priority = 1;
>            yield return new WaitForSeconds(4);
>
>            index++;
>        }
>    }
> ```  
> 
> Este script sólo funciona si estamos en modo replay y pulsamos la barra espaciadora.

</div>
  

## **Extras**

<div style="text-align: justify"> 

Lamentablemente, por falta de tiempo, me ha sido imposible refactorizar las clases **Ghost Manager** y **Replay Manager**. Ambas tienen una implementación muy similar y podrían simplemente llamar a métodos pertenecientes a una clase externa que gestionen el guardado y la lectura de los datos de las vueltas. Dicho refactor se intentó realizar sin éxito, aunque estoy prácticamente convencido que los fallos producidos al intentar unificar código se debían a la nula relación entre muestras y tiempo de juego. Para una correcta implementación, cada muestra de juego debería ir asociada a un tiempo de guardado para evitar problemas con la frecuencia de muestreo. 

</div>