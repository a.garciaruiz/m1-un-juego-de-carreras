using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class GhostManager : MonoBehaviour
{
    public float timeBetweenSamples = 0.25f;
    public GameObject carToRecord;
    public GameObject carToPlay;

    // SAVE VARIABLES
    public static List<GhostData> ghostData = new List<GhostData>();

    // RECORD VARIABLES
    public static bool shouldRecord = false;
    private static float totalRecordedTime = 0.0f;
    private static float currenttimeBetweenSamples = 0.0f;

    // REPLAY VARIABLES
    public static bool shouldPlay = false;
    private static float totalPlayedTime = 0.0f;
    private static float currenttimeBetweenPlaySamples = 0.0f;
    private static int currentSampleToPlay = 0;

    // POSITIONS/ROTATIONS
    private Vector3 lastSamplePosition = Vector3.zero;
    private Quaternion lastSampleRotation = Quaternion.identity;
    private Vector3 nextPosition = Vector3.zero;
    private Quaternion nextRotation = Quaternion.identity;

    public static void StartRecording()
    {
        Debug.Log("START RECORDING");
        shouldRecord = true;
        shouldPlay = false;

        // Seteamos los valores iniciales
        totalRecordedTime = 0;
        currenttimeBetweenSamples = 0;
        ghostData.Clear();
    }

    public static void StartPlaying()
    {
        Debug.Log("START PLAYING");
        shouldPlay = true;

        // Seteamos los valores iniciales
        totalPlayedTime = 0;
        currentSampleToPlay = 0;
        currenttimeBetweenPlaySamples = 0;
    }

    private void Update()
    {
        // Si el tiempo transcurrido es mayor que el tiempo de muestreo
        if (shouldRecord && ReplayManager.replayMode == false)
        {
            // A cada frame incrementamos el tiempo transcurrido 
            totalRecordedTime += Time.deltaTime;
            currenttimeBetweenSamples += Time.deltaTime;

            if (currenttimeBetweenSamples >= timeBetweenSamples)
            {
                // Guardamos la información para el fantasma
                ghostData.Add(new GhostData(carToRecord.transform.position, carToRecord.transform.rotation));
                // Dejamos el tiempo extra entre una muestra y otra
                currenttimeBetweenSamples -= timeBetweenSamples;
            }
        }

        if (shouldPlay && ReplayManager.replayMode == false)
        {
            // A cada frame incrementamos el tiempo transcurrido 
            totalPlayedTime += Time.deltaTime;
            currenttimeBetweenPlaySamples += Time.deltaTime;

            // Si el tiempo transcurrido es mayor que el tiempo de muestreo
            if (currenttimeBetweenPlaySamples >= timeBetweenSamples)
            {
                // De cara a interpolar de una manera fluida la posición del coche entre una muestra y otra,
                // guardamos la posición y la rotación de la anterior muestra
                lastSamplePosition = nextPosition;
                lastSampleRotation = nextRotation;

                // Cogemos los datos del scriptable object
                carToPlay.SetActive(true);
                //Debug.Log(currentSampleToPlay);
                nextPosition = LoadData.ghostData[currentSampleToPlay].ghostPos;
                nextRotation = LoadData.ghostData[currentSampleToPlay].ghostRot;

                // Dejamos el tiempo extra entre una muestra y otra
                currenttimeBetweenPlaySamples -= timeBetweenSamples;

                // Incrementamos el contador de muestras
                currentSampleToPlay++;

                if (currentSampleToPlay == LoadData.ghostData.Count)
                {
                    currentSampleToPlay = 0;
                }
            }

            // De cara a crear una interpolación suave entre la posición y rotación entre una muestra y la otra, 
            // calculamos a nivel de tiempo entre muestras el porcentaje en el que nos encontramos
            float percentageBetweenFrames = currenttimeBetweenPlaySamples / timeBetweenSamples;

            // Aplicamos un lerp entre las posiciones y rotaciones de la muestra anterior y la siguiente según el procentaje actual.
            carToPlay.transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
            carToPlay.transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
        }
    }

    public static void UpdateGhost()
    {
        FileHandler.SaveToJSON<GhostData>(ghostData, Files.ghostDataFile);
    }
}

