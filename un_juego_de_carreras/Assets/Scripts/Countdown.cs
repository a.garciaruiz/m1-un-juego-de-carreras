using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class Countdown : MonoBehaviour
{
    private int countdown_time = 3;
    private GameObject countdown;
    private GameObject lap_timer;

    private Rigidbody car_rb;

    private AudioSource countdown_audio;
    private AudioSource start_audio;

    void Start()
    {
        countdown = GameObject.Find("CountdownUI");
        lap_timer = GameObject.Find("LapTimeManager");
        countdown_audio = GameObject.Find("countdown_audio").GetComponent<AudioSource>();
        start_audio = GameObject.Find("start_audio").GetComponent<AudioSource>();
        car_rb = GameObject.Find("Car").GetComponent<Rigidbody>();

        countdown.SetActive(false);
        lap_timer.SetActive(false);
        car_rb.constraints = RigidbodyConstraints.FreezePosition;

        StartCoroutine(CountdownStart());
    }

    IEnumerator CountdownStart()
    {
        yield return new WaitForSeconds(0.5f);

        while(countdown_time > 0)
        {
            countdown.SetActive(true);
            countdown.GetComponent<Text>().text = countdown_time.ToString();
            countdown_audio.Play();
            yield return new WaitForSeconds(1f);
            countdown.SetActive(false);
            countdown_time--;
        }

        yield return new WaitForSeconds(0.5f);
        countdown.SetActive(false);
        start_audio.Play();

        lap_timer.SetActive(true);
        car_rb.constraints = RigidbodyConstraints.None;

        GhostManager.StartRecording();

        if (File.Exists(FileHandler.GetPath(Files.ghostDataFile))) {
            GhostManager.StartPlaying();
        }
    }
}
