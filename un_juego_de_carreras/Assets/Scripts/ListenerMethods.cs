using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ListenerMethods
{
    // Restaura los valores si se ha parado el juego
    public static void ResumeGame(GameObject obj, AudioSource[] audio)
    {
        obj.SetActive(false);
        RestoreTime();
        GhostManager.shouldRecord = true;
        foreach (AudioSource audioSource in audio)
        {
            audioSource.enabled = true;
        }
    }

    // Cambia de escena
    public static void ChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
        ReplayManager.replayMode = false;
        RestoreTime();
    }

    // Activa y desactiva paneles para evitar superposición
    public static void ChangePanel(GameObject activeObj, GameObject inactiveObj)
    {
        activeObj.SetActive(false);
        inactiveObj.SetActive(true);
    }

    // Cierra la app
    public static void QuitApp()
    {
        Debug.Log("APPLICATION CLOSED!");
        Application.Quit();
    }

    // Elimina los listeners
    public static void RemoveListeners(List<Button> buttons)
    {
        foreach (Button button in buttons)
        {
            button.onClick.RemoveAllListeners();
        }
    }

    // Restablece el tiempo de juego
    private static void RestoreTime()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }
}
