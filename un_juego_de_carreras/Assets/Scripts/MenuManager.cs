using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private GameObject startMenu;
    private GameObject tracksMenu;

    private Button trackSelectionButt;
    private Button quitButt;
    private Button backButton;
    private Button road1;

    private List<Button> startMenuButtons = new List<Button>();
    private List<Button> tracksMenuButtons = new List<Button>();

    void Start()
    {
        // Recogemos los objetos necesarios
        startMenu = GameObject.Find("StartPanel");
        tracksMenu = GameObject.Find("TrackSelectionPanel");

        trackSelectionButt = GameObject.Find("TrackSelectionButton").GetComponent<Button>();
        quitButt = GameObject.Find("QuitButton").GetComponent<Button>();
        backButton = GameObject.Find("BackArrow").GetComponent<Button>();
        road1 = GameObject.Find("Road1").GetComponent<Button>();

        // Añadimos botones a la lista para eliminar los listeners
        startMenuButtons.Add(trackSelectionButt);
        startMenuButtons.Add(quitButt);
        tracksMenuButtons.Add(backButton);
        tracksMenuButtons.Add(road1);

        // Desactivamos el panel principal y activamos el panel de selección de circuito
        trackSelectionButt.onClick.AddListener(() => ListenerMethods.ChangePanel(startMenu, tracksMenu));
        quitButt.onClick.AddListener(() => ListenerMethods.QuitApp());

        // Desactivamos el panel de selección de circuito y activamos el principal
        backButton.onClick.AddListener(() => ListenerMethods.ChangePanel(tracksMenu, startMenu));

        // Cargamos la escena seleccionada
        road1.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.road1Scene));

        // Desactivamos el panel de selección de circuito
        tracksMenu.SetActive(false);
    }

    private void OnDestroy()
    {
        ListenerMethods.RemoveListeners(startMenuButtons);
        ListenerMethods.RemoveListeners(tracksMenuButtons);
    }
}
