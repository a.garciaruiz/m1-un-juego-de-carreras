using UnityEngine;

public class LapTimeManager : MonoBehaviour
{
    [HideInInspector]public static int min_count = 0;
    [HideInInspector] public static int sec_count = 0;
    [HideInInspector] public static float ms_count = 0;
    public static string ms_display;

    private GameObject min_box;
    private GameObject sec_box;
    private GameObject ms_box;

    private void Start()
    {
        min_box = GameObject.Find("MinDisplay");
        sec_box = GameObject.Find("SecDisplay");
        ms_box = GameObject.Find("MsDisplay");
    }

    private void Update()
    {
        if(LapCounter.lap < LapCounter.max_lap) {
            ms_count += Time.deltaTime * 10;
            UpdateDisplays.UpdateMsDisplay(ms_box, ms_count);

            UpdateSecs();
            UpdateMins();
        }

    }

    private void UpdateSecs()
    {
        if (ms_count >= 10)
        {
            ms_count = 0;
            sec_count += 1;
        }
        UpdateDisplays.UpdateSecsDisplay(sec_box, sec_count);
    }

    private void UpdateMins()
    {
        if (sec_count >= 60)
        {
            sec_count = 0;
            min_count += 1;
        }
        UpdateDisplays.UpdateMinsDisplay(min_box, min_count);
    }
}
