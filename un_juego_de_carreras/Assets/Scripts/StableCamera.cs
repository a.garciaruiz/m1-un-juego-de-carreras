using UnityEngine;

public class StableCamera : MonoBehaviour
{
    private GameObject _car;
    [HideInInspector] public float _carX;
    private float _carY;
    [HideInInspector] public float _carZ;


    private void Start()
    {
        _car = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        _carX = _car.transform.eulerAngles.x;
        _carY = _car.transform.eulerAngles.y;
        _carZ = _car.transform.eulerAngles.z;

        transform.eulerAngles = new Vector3(_carX - _carX, _carY, _carZ - _carZ);
    }
}
