using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateNextTrigger : MonoBehaviour
{
    public GameObject[] triggers;

    private void OnTriggerEnter(Collider other)
    {
        // Activa el siguiente checkpoint y desactiva el que se acaba de cruzar
        if (other.gameObject.CompareTag("Player"))
        {
            triggers[0].SetActive(false);
            triggers[1].SetActive(true);
        }
    }
}
