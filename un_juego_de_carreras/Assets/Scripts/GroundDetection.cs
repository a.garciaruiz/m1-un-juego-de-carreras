using UnityEngine;

public class GroundDetection : MonoBehaviour
{
    public LayerMask roadLayer;
    public LayerMask terrainLayer;
    private GameObject car;
    private GameObject activeCheckpoint;
    private Rigidbody rb;
    private float timer;

    private void Start()
    {
        car = GameObject.FindGameObjectWithTag("Player");
        rb = gameObject.GetComponent<Rigidbody>();
        timer = 0;
    }

    private void FixedUpdate()
    {
        activeCheckpoint = GameObject.FindGameObjectWithTag("Checkpoint");
        RaycastHit hit;
        if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 10, roadLayer))
        {
            rb.drag = 0.1f;
            timer = 0;
        } else if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 10, terrainLayer))
        {
            rb.drag = 0.5f;
            timer += Time.deltaTime;
            if (timer >= 5)
            {
                car.transform.position = activeCheckpoint.transform.position;
                car.transform.rotation = activeCheckpoint.transform.rotation;
                timer = 0;
            }

        }
        else
        {
            rb.AddForce(rb.mass * Physics.gravity);
        }
    }
}

