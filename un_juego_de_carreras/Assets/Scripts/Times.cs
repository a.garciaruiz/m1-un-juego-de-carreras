using System;

[Serializable]
public class Times
{
    public int min;
    public int sec;
    public float ms;

    public Times(int min, int sec, float ms)
    {
        this.min = min;
        this.sec = sec;
        this.ms = ms;
    }
}
