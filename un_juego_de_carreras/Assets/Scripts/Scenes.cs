using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenes
{
    // Script que contiene las escenas del juego para su acceso en toda la app(ampliable en caso de añadir nuevas)
    public static string mainScene = "Main";
    public static string road1Scene = "Road1";
}
