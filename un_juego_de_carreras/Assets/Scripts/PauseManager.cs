using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Vehicles.Car;

public class PauseManager : MonoBehaviour
{
    private Scene scene;
    private GameObject pausePanel;

    private Button resumeButton;
    private Button restartButton;
    private Button exitButton;
    private List<Button> menuButtons = new List<Button>();

    private GameObject car;
    private AudioSource[] carAudio;

    void Start()
    {
        scene = SceneManager.GetActiveScene();

        // Recogemos los objetos necesarios
        pausePanel = GameObject.Find("PausePanel");

        resumeButton = GameObject.Find("ResumeButton").GetComponent<Button>();
        restartButton = GameObject.Find("RestartButton").GetComponent<Button>();
        exitButton = GameObject.Find("ExitButton").GetComponent<Button>();

        car = GameObject.FindGameObjectWithTag("Player");

        // Añadimos los botones a la lista para eliminar los listeners
        menuButtons.Add(resumeButton);
        menuButtons.Add(restartButton);
        menuButtons.Add(exitButton);

        resumeButton.onClick.AddListener(() => ListenerMethods.ResumeGame(pausePanel, carAudio));
        restartButton.onClick.AddListener(() => ListenerMethods.ChangeScene(scene.name));
        exitButton.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.mainScene));

        // Desactivamos el panel de pausa
        pausePanel.SetActive(false);
    }

    void Update()
    {
        // Si apretamos ESC
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // Inhabilitamos el audio del coche
            carAudio = car.GetComponents<AudioSource>();
            foreach (AudioSource audioSource in carAudio)
            {
                audioSource.enabled = false;
            }

            // Paramos el juego
            Time.timeScale = 0;
            GhostManager.shouldRecord = false;

            // Activamos el panel de pausa
            pausePanel.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        // Eliminamos los listeners
        ListenerMethods.RemoveListeners(menuButtons);
    }
}
