using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Files
{
    // Contiene los nombres de los archivos que se guardan en memoria (ampliable)
    public static string ghostDataFile = "ghostData.json";
    public static string timesFile = "best_time.json";
}
