using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplayManager : MonoBehaviour
{
    public static bool replayMode;
    private GameObject finish_panel;
    public GameObject ghostCar;

    private List<GhostData> replayData = new List<GhostData>();

    public static float timeBetweenSamples = 0.25f;

    // RECORD VARIABLES
    public static bool shouldRecord = false;
    private static float totalRecordedTime = 0.0f;
    public static float currenttimeBetweenSamples = 0.0f;

    // REPLAY VARIABLES
    public static bool shouldPlay = false;
    private static float totalPlayedTime = 0.0f;
    public static float currenttimeBetweenPlaySamples = 0.0f;
    public static int currentSampleToPlay = 0;

    // POSITIONS/ROTATIONS
    private Vector3 lastSamplePosition = Vector3.zero;
    private Quaternion lastSampleRotation = Quaternion.identity;
    private Vector3 nextPosition = Vector3.zero;
    private Quaternion nextRotation = Quaternion.identity;

    private void Start()
    {
        finish_panel = GameObject.Find("FinishPanel");
        replayMode = false;
    }

    private void FixedUpdate()
    {
        // A cada frame incrementamos el tiempo transcurrido 
        totalRecordedTime += Time.deltaTime;
        currenttimeBetweenSamples += Time.deltaTime;

        if (replayMode == false && !RaceFinished.race_finished)
        {
            if (currenttimeBetweenSamples >= timeBetweenSamples)
            {
                replayData.Add(new GhostData(transform.position, transform.rotation));
                currenttimeBetweenSamples -= timeBetweenSamples;
            }
        }

        if (replayMode)
        {
            ghostCar.SetActive(false);

            if (currentSampleToPlay < replayData.Count)
            {
                // A cada frame incrementamos el tiempo transcurrido 
                totalPlayedTime += Time.deltaTime;
                currenttimeBetweenPlaySamples += Time.deltaTime;

                // Si el tiempo transcurrido es mayor que el tiempo de muestreo
                if (currenttimeBetweenPlaySamples >= timeBetweenSamples)
                {
                    // De cara a interpolar de una manera fluida la posición del coche entre una muestra y otra,
                    // guardamos la posición y la rotación de la anterior muestra
                    lastSamplePosition = nextPosition;
                    lastSampleRotation = nextRotation;

                    //Debug.Log(currentSampleToPlay);
                    nextPosition = replayData[currentSampleToPlay].ghostPos;
                    nextRotation = replayData[currentSampleToPlay].ghostRot;

                    // Dejamos el tiempo extra entre una muestra y otra
                    currenttimeBetweenPlaySamples -= timeBetweenSamples;

                    // Incrementamos el contador de muestras
                    currentSampleToPlay++;
                }

                // De cara a crear una interpolación suave entre la posición y rotación entre una muestra y la otra, 
                // calculamos a nivel de tiempo entre muestras el porcentaje en el que nos encontramos
                float percentageBetweenFrames = currenttimeBetweenPlaySamples / timeBetweenSamples;

                // Aplicamos un lerp entre las posiciones y rotaciones de la muestra anterior y la siguiente según el procentaje actual.
                transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
                transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
            }
            else
            {
                replayMode = false;
                currentSampleToPlay = 0;
                finish_panel.SetActive(true);
                RaceFinished.EnableAI();
            }
        }
    }
}
