using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineManager : MonoBehaviour
{
    private GameObject main_camera;
    public static GameObject cinemachine_brain;

    private List<CinemachineVirtualCamera> vcams = new List<CinemachineVirtualCamera>();

    [SerializeField] private CinemachineVirtualCamera vcam1;
    [SerializeField] private CinemachineVirtualCamera vcam2;
    [SerializeField] private CinemachineVirtualCamera vcam3;
    [SerializeField] private CinemachineVirtualCamera vcam4;

    private int index = 0;

    private bool active = true;

    void Start()
    {
        main_camera = GameObject.Find("Main Camera");
        cinemachine_brain = GameObject.Find("CinemachineBrain");

        vcams.Add(vcam1);
        vcams.Add(vcam2);
        vcams.Add(vcam3);
        vcams.Add(vcam4);

        StartCoroutine(SwitchPriority());

        cinemachine_brain.SetActive(!active);
    }

    void Update()
    {
        if (ReplayManager.replayMode)
        {
            if(main_camera.activeSelf == true)
            {
                cinemachine_brain.GetComponent<AudioListener>().enabled = false;
            }
            else
            {
                cinemachine_brain.GetComponent<AudioListener>().enabled = true;
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                main_camera.SetActive(!active);
                cinemachine_brain.SetActive(active);
                active = !active;
            }
        }
    }

    IEnumerator SwitchPriority()
    {
        while (true)
        {
            if (index >= 4)
            {
                index = 0;
            }

            foreach (CinemachineVirtualCamera vcam in vcams)
            {
                vcam.Priority = 0;
            }
            vcams[index].Priority = 1;
            yield return new WaitForSeconds(4);

            index++;
        }
    }
}
