using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LapCompleted : MonoBehaviour
{
    [SerializeField] private GameObject[] lap1_display;
    [SerializeField] private GameObject[] lap2_display;
    [SerializeField] private GameObject[] best_display;

    private List<Times> bestTimes;

    private void Start()
    {
        if(!File.Exists(FileHandler.GetPath(Files.timesFile)))
        {
            bestTimes = new List<Times>();
        }
        else
        {
            bestTimes = FileHandler.ReadFromJSON<Times>(Files.timesFile);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        LapCheck();
        ResetTimer();
        GhostManager.ghostData.Clear();
    }

    private void LapCheck()
    {
        if (ReplayManager.replayMode == false && RaceFinished.race_finished == false)
        {
            if (LapCounter.lap == 0)
            {
                UpdateDisplays.UpdateMinsDisplay(lap1_display[0], LapTimeManager.min_count);
                UpdateDisplays.UpdateSecsDisplay(lap1_display[1], LapTimeManager.sec_count);
                UpdateDisplays.UpdateMsDisplay(lap1_display[2], LapTimeManager.ms_count);

                if (!File.Exists(FileHandler.GetPath(Files.timesFile)))
                {
                    UpdateBestTime(LapTimeManager.min_count, LapTimeManager.sec_count, LapTimeManager.ms_count);
                    UpdateDisplays.UpdateBestDisplay(best_display, LapTimeManager.min_count, LapTimeManager.sec_count, LapTimeManager.ms_count);

                    GhostManager.UpdateGhost();
                }
                else
                {
                    UpdateBestLap(LapTimeManager.min_count, LapTimeManager.sec_count, LapTimeManager.ms_count);
                }
            }
            else if (LapCounter.lap == 1)
            {
                UpdateDisplays.UpdateMinsDisplay(lap2_display[0], LapTimeManager.min_count);
                UpdateDisplays.UpdateSecsDisplay(lap2_display[1], LapTimeManager.sec_count);
                UpdateDisplays.UpdateMsDisplay(lap2_display[2], LapTimeManager.ms_count);

                UpdateBestLap(LapTimeManager.min_count, LapTimeManager.sec_count, LapTimeManager.ms_count);
            }
            else if (LapCounter.lap == 2)
            {
                UpdateBestLap(LapTimeManager.min_count, LapTimeManager.sec_count, LapTimeManager.ms_count);
            }
        }
    }

    private void UpdateBestLap(int new_min, int new_sec, float new_ms)
    {
        if (ReplayManager.replayMode == false && RaceFinished.race_finished == false)
        {
            if (new_min < bestTimes[0].min ||
               (new_min <= bestTimes[0].min && new_sec < bestTimes[0].sec) ||
               (new_min <= bestTimes[0].min && new_sec <= bestTimes[0].sec && new_ms < bestTimes[0].ms))
            {
                UpdateDisplays.UpdateBestDisplay(best_display, new_min, new_sec, new_ms);
                UpdateBestTime(new_min, new_sec, new_ms);
                GhostManager.UpdateGhost();
            }
        }
    }
        
    private void UpdateBestTime(int new_best_min, int new_best_sec, float new_best_ms)
    {
        bestTimes.Clear();
        bestTimes.Add(new Times(new_best_min, new_best_sec, new_best_ms));
        FileHandler.SaveToJSON<Times>(bestTimes, Files.timesFile);
    }

    private void ResetTimer()
    {
        LapTimeManager.min_count = 0;
        LapTimeManager.sec_count = 0;
        LapTimeManager.ms_count = 0;
    }
}
