using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Vehicles.Car;
using UnityStandardAssets.Utility;

public class RaceFinished : MonoBehaviour
{
    private Scene scene;

    public static bool race_finished = false;

    public static GameObject car;
    private GameObject finish_camera;
    private GameObject finish_cube;
    private GameObject main_camera;

    private GameObject finish_panel;
    private Button restart_button;
    private Button replay_button;
    private Button exit_button;
    private List<Button> menuButtons = new List<Button>();

    private AudioSource[] audio_sources;

    private void Start()
    {
        scene = SceneManager.GetActiveScene();

        // Recogemos los objetos del coche y las cámaras
        car = GameObject.FindGameObjectWithTag("Player");
        finish_camera = GameObject.Find("FinishCamera");
        finish_cube = GameObject.Find("FinishCube");
        main_camera = GameObject.FindGameObjectWithTag("MainCamera");

        // Recogemos los objetos del panel de finalización
        finish_panel = GameObject.Find("FinishPanel");
        restart_button = GameObject.Find("TryAgainButton").GetComponent<Button>();
        replay_button = GameObject.Find("ReplayButton").GetComponent<Button>();
        exit_button = GameObject.Find("ExitButton").GetComponent<Button>();

        // Añadimos los botones a una lista para eliminar los listeners en OnDestroy
        menuButtons.Add(restart_button);
        menuButtons.Add(replay_button);
        menuButtons.Add(exit_button);

        restart_button.onClick.AddListener(() => ListenerMethods.ChangeScene(scene.name));
        replay_button.onClick.AddListener(() => EnterReplay());
        exit_button.onClick.AddListener(() => ListenerMethods.ChangeScene(Scenes.mainScene));

        // Desactivamos hasta que entramos en el trigger
        finish_panel.SetActive(false);
        finish_cube.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (LapCounter.lap == LapCounter.max_lap)
        {
            race_finished = true;

            //Paramos el fantasma y la grabación
            Debug.Log("STOP RECORDING");
            GhostManager.shouldRecord = false;
            Debug.Log("STOP PLAYING");
            GhostManager.shouldPlay = false;

            //Activamos la camara y el panel de finalización
            finish_cube.SetActive(true);
            finish_panel.SetActive(true);

            //Activamos la inteligencia artificial y desactivamos los controles de usuario
            EnableAI();
            DisableCarSettings();

            //Activamos la camara de final de carrera
            main_camera.GetComponent<AudioListener>().enabled = false;
            finish_camera.SetActive(true);
            main_camera.SetActive(false);
        }
    }

    private void DisableCarSettings()
    {
        car.GetComponent<CarUserControl>().enabled = false;
        audio_sources = car.GetComponents<AudioSource>();

        foreach(AudioSource audioSource in audio_sources)
        {
            audioSource.enabled = false;
        }
        car.GetComponent<CarAudio>().enabled = false;
    }

    public static void EnableAI()
    {
        car.GetComponent<CarAIControl>().enabled = true;
        car.GetComponent<WaypointProgressTracker>().enabled = true;
    }

    private void EnterReplay()
    {
        ReplayManager.replayMode = true;

        DisableCarSettings();

        GetComponent<BoxCollider>().enabled = false;
        finish_panel.SetActive(false);
        finish_cube.SetActive(false);

        if(CinemachineManager.cinemachine_brain.activeSelf == false) {
            main_camera.GetComponent<AudioListener>().enabled = true;
        }
        finish_camera.GetComponent<AudioListener>().enabled = false;
        main_camera.SetActive(true);
        finish_camera.SetActive(false);

        car.GetComponent<CarAIControl>().enabled = false;
        car.GetComponent<WaypointProgressTracker>().enabled = false;
    }

    private void OnDestroy()
    {
        ListenerMethods.RemoveListeners(menuButtons);
    }
}
