using UnityEngine;
using UnityEngine.UI;

public class LapCounter : MonoBehaviour
{
    [HideInInspector] public static int lap;
    [HideInInspector] public static int max_lap = 3;
    private GameObject lap_counter;
    

    void Start()
    {
        lap_counter = GameObject.Find("LapCounter");
        lap = 0;
    }

    private void Update()
    {
        if (lap < max_lap)
        {
            lap_counter.GetComponent<Text>().text = (lap + 1) + "/3";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (lap < max_lap)
        {
            lap++;
        }
    }
}
