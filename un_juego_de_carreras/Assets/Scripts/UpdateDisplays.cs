using UnityEngine;
using UnityEngine.UI;

public static class UpdateDisplays
{
    public static void UpdateMinsDisplay(GameObject display, int mins)
    {
        if (mins <= 9)
        {
            display.GetComponent<Text>().text = "0" + mins + ":";
        }
        else
        {
            display.GetComponent<Text>().text = "" + mins + ":";
        }
    }

    public static void UpdateSecsDisplay(GameObject display, int secs)
    {
        if (secs <= 9)
        {
            display.GetComponent<Text>().text = "0" + secs + ".";
        }
        else
        {
            display.GetComponent<Text>().text = "" + secs + ".";
        }
    }

    public static void UpdateMsDisplay(GameObject display, float ms)
    {
        display.GetComponent<Text>().text = "" + ms;
    }

    public static void UpdateBestDisplay(GameObject[] display, int mins, int secs, float ms)
    {
        UpdateMinsDisplay(display[0], mins);
        UpdateSecsDisplay(display[1], secs);
        UpdateMsDisplay(display[2], ms);
    }
}
