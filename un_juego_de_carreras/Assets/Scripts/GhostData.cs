using UnityEngine;
using System;

[Serializable]
public class GhostData
{
    public Vector3 ghostPos;
    public Quaternion ghostRot;

    public GhostData(Vector3 ghostPos, Quaternion ghostRot)
    {
        this.ghostPos = ghostPos;
        this.ghostRot = ghostRot;
    }
}
