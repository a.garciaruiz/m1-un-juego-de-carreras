using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadData : MonoBehaviour
{
    public static List<Times> bestTimes;
    public static List<GhostData> ghostData;
    [SerializeField] private GameObject[] best_display;

    void Start()
    {
        if (File.Exists(FileHandler.GetPath(Files.timesFile)))
        {
            bestTimes = FileHandler.ReadFromJSON<Times>(Files.timesFile);
            UpdateDisplays.UpdateBestDisplay(best_display, bestTimes[0].min, bestTimes[0].sec, bestTimes[0].ms);
        }

        if (File.Exists(FileHandler.GetPath(Files.ghostDataFile)))
        {
            ghostData = FileHandler.ReadFromJSON<GhostData>(Files.ghostDataFile);
        }

    }
}
